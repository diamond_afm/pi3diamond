import threading

import numpy
from chaco.api import ArrayPlotData
from enable.api import ComponentEditor
# enthought library imports
from traits.api import Instance, Range, Bool, Array, on_trait_change, String
from traitsui.api import Handler, View, Item, HGroup, Group

from tools.chaco_addons import SavePlot as Plot
from tools.emod import Job
from tools.utility import GetSetItemsMixin


class StartThreadHandler(Handler):
    def init(self, info):
        info.object.start()


class PhotonTimeTrace(Job, GetSetItemsMixin):
    TraceLength = Range(low=10, high=10000, value=400, desc='Length of Count Trace',
                        label='Trace Length')
    SecondsPerPoint = Range(low=0.001, high=1, value=0.1, desc='Seconds per point [s]',
                            label='Seconds per point [s]')
    RefreshRate = Range(low=0.01, high=1, value=0.1, desc='Refresh rate [s]',
                        label='Refresh rate [s]')
    TotalCounts = String("")

    # trace data
    C0 = Array()
    C1 = Array()
    C2 = Array()
    C3 = Array()
    C4 = Array()
    C5 = Array()
    C6 = Array()
    C7 = Array()
    C0C1 = Array()
    T = Array()

    c_enable0 = Bool(True, label='C0', desc='enable channel 0')
    c_enable1 = Bool(True, label='C1', desc='enable channel 1')
    c_enable2 = Bool(False, label='C2', desc='enable channel 2')
    c_enable3 = Bool(False, label='C3', desc='enable channel 3')
    c_enable4 = Bool(False, label='C4', desc='enable channel 4')
    c_enable5 = Bool(False, label='C5', desc='enable channel 5')
    c_enable6 = Bool(False, label='C6', desc='enable channel 6')
    c_enable7 = Bool(False, label='C7', desc='enable channel 7')
    sum_enable = Bool(True, label='C0 + C1', desc='enable sum c0 + c1')

    TracePlot = Instance(Plot)
    TraceData = Instance(ArrayPlotData)

    def __init__(self, time_tagger, **kwargs):
        super(PhotonTimeTrace, self).__init__(**kwargs)
        self.time_tagger = time_tagger
        self.on_trait_change(self._update_T, 'T', dispatch='ui')
        self.on_trait_change(self._update_C0, 'C0', dispatch='ui')
        self.on_trait_change(self._update_C1, 'C1', dispatch='ui')
        self.on_trait_change(self._update_C2, 'C2', dispatch='ui')
        self.on_trait_change(self._update_C3, 'C3', dispatch='ui')
        self.on_trait_change(self._update_C4, 'C4', dispatch='ui')
        self.on_trait_change(self._update_C5, 'C5', dispatch='ui')
        self.on_trait_change(self._update_C6, 'C6', dispatch='ui')
        # self.on_trait_change(self._update_C7, 'C7', dispatch='ui')
        self.on_trait_change(self._update_C0C1, 'C0C1', dispatch='ui')
        self._create_counter()

    def _counts_to_string(self, counts):
        """Transforms a number into a String, whose format depends on the size of the number."""
        if 9.95e2 <= counts:
            string = ('%5.2f' % (counts / 1000.))[:5] + ' k'
        else:
            string = ('%5.1f' % counts)[:5]
        return string

    def _create_counter(self):
        self._counter0 = self.time_tagger.Counter(0, int(self.SecondsPerPoint * 1e12),
                                                  self.TraceLength)
        self._counter1 = self.time_tagger.Counter(1, int(self.SecondsPerPoint * 1e12),
                                                  self.TraceLength)
        self._counter2 = self.time_tagger.Counter(2, int(self.SecondsPerPoint * 1e12),
                                                  self.TraceLength)
        self._counter3 = self.time_tagger.Counter(3, int(self.SecondsPerPoint * 1e12),
                                                  self.TraceLength)
        self._counter4 = self.time_tagger.Counter(4, int(self.SecondsPerPoint * 1e12),
                                                  self.TraceLength)
        self._counter5 = self.time_tagger.Counter(5, int(self.SecondsPerPoint * 1e12),
                                                  self.TraceLength)
        self._counter6 = self.time_tagger.Counter(6, int(self.SecondsPerPoint * 1e12),
                                                  self.TraceLength)
        # self._counter7 = self.time_tagger.Counter(7, int(self.SecondsPerPoint*1e12), self.TraceLength) # ToDo: does not work when using channel 7

    def _C0_default(self):
        return numpy.zeros((self.TraceLength,))

    def _C1_default(self):
        return numpy.zeros((self.TraceLength,))

    def _C2_default(self):
        return numpy.zeros((self.TraceLength,))

    def _C3_default(self):
        return numpy.zeros((self.TraceLength,))

    def _C4_default(self):
        return numpy.zeros((self.TraceLength,))

    def _C5_default(self):
        return numpy.zeros((self.TraceLength,))

    def _C6_default(self):
        return numpy.zeros((self.TraceLength,))

    def _C7_default(self):
        return numpy.zeros((self.TraceLength,))

    def _C0C1_default(self):
        return numpy.zeros((self.TraceLength,))

    def _T_default(self):
        return self.SecondsPerPoint * numpy.arange(self.TraceLength)

    def _update_T(self):
        self.TraceData.set_data('t', self.T)

    def _update_plot_title(self):
        self.TracePlot.title = self._counts_to_string(self.C0C1[-1])
        self.TotalCounts = self._counts_to_string(self.C0C1[-1])
        resize_param = int(self.TracePlot.width / 10 + self.TracePlot.height / 10)
        self.TracePlot.title_font.size = resize_param
        self.TracePlot.padding = [50, 10, resize_param, 10]
        self.TracePlot.request_redraw()

    def _update_C0(self):
        self.TraceData.set_data('y0', self.C0)
        self._update_plot_title()

    def _update_C1(self):
        self.TraceData.set_data('y1', self.C1)
        # self.TracePlot.request_redraw()

    def _update_C2(self):
        self.TraceData.set_data('y2', self.C2)
        # self.TracePlot.request_redraw()

    def _update_C3(self):
        self.TraceData.set_data('y3', self.C3)
        # self.TracePlot.request_redraw()

    def _update_C4(self):
        self.TraceData.set_data('y4', self.C4)
        # self.TracePlot.request_redraw()

    def _update_C5(self):
        self.TraceData.set_data('y5', self.C5)
        # self.TracePlot.request_redraw()

    def _update_C6(self):
        self.TraceData.set_data('y6', self.C6)
        # self.TracePlot.request_redraw()

    def _update_C7(self):
        self.TraceData.set_data('y7', self.C7)
        # self.TracePlot.request_redraw()

    def _update_C0C1(self):
        self.TraceData.set_data('y8', self.C0C1)
        # self.TracePlot.request_redraw()

    def _TraceLength_changed(self):
        self.C0 = self._C0_default()
        self.C1 = self._C1_default()
        self.C2 = self._C2_default()
        self.C3 = self._C3_default()
        self.C4 = self._C4_default()
        self.C5 = self._C5_default()
        self.C6 = self._C6_default()
        self.C7 = self._C7_default()
        self.C0C1 = self._C0C1_default()
        self.T = self._T_default()
        self._create_counter()

    def _SecondsPerPoint_changed(self):
        self.T = self._T_default()
        self._create_counter()

    def _TraceData_default(self):
        return ArrayPlotData(t=self.T, y0=self.C0, y1=self.C1, y2=self.C2, y3=self.C3, y4=self.C4,
                             y5=self.C5, y6=self.C6, y7=self.C7, y8=self.C0C1)

    def _TracePlot_default(self):
        return self._replot()

    @on_trait_change('c_enable0,c_enable1,c_enable2,c_enable3,c_enable4,c_enable5,c_enable6,sum_enable')
    def _replot(self):
        self.TracePlot = Plot(self.TraceData, resizable='hv')
        self.TracePlot.legend.align = 'll'

        n = 0
        if self.c_enable0:
            self.TracePlot.plot(('t', 'y0'), type='line', color='blue', name='C0',
                                line_width=2)
            n += 1
        if self.c_enable1:
            self.TracePlot.plot(('t', 'y1'), type='line', color='red', name='C1',
                                line_width=2)
            n += 1
        if self.c_enable2:
            self.TracePlot.plot(('t', 'y2'), type='line', color='green', name='C2',
                                line_width=2)
            n += 1
        if self.c_enable3:
            self.TracePlot.plot(('t', 'y3'), type='line', color='black', name='C3')
            n += 1
        if self.c_enable4:
            self.TracePlot.plot(('t', 'y4'), type='line', color='blue', name='C4')
            n += 1
        if self.c_enable5:
            self.TracePlot.plot(('t', 'y5'), type='line', color='red', name='C5')
            n += 1
        if self.c_enable6:
            self.TracePlot.plot(('t', 'y6'), type='line', color='green', name='C6')
            n += 1
        # if self.c_enable7:
        #    self.TracePlot.plot(('t','y7'), type='line', color='black', name='channel 7')
        if self.sum_enable:
            self.TracePlot.plot(('t', 'y8'), type='line', color='black', name='C0 + C1',
                                line_width=2)
            n += 1

        if n > 1:
            self.TracePlot.legend.visible = True
        else:
            self.TracePlot.legend.visible = False

        return self.TracePlot

    def _run(self):
        """Acquire Count Trace"""
        while True:
            threading.current_thread().stop_request.wait(self.RefreshRate)
            if threading.current_thread().stop_request.isSet():
                break

            self.C0 = self._counter0.getData() / self.SecondsPerPoint
            self.C1 = self._counter1.getData() / self.SecondsPerPoint
            self.C2 = self._counter2.getData() / self.SecondsPerPoint
            self.C3 = self._counter3.getData() / self.SecondsPerPoint
            self.C4 = self._counter4.getData() / self.SecondsPerPoint
            self.C5 = self._counter5.getData() / self.SecondsPerPoint
            self.C6 = self._counter6.getData() / self.SecondsPerPoint
            # self.C7 = self._counter7.getData() / self.SecondsPerPoint
            self.C0C1 = self.C0 + self.C1

    get_set_items = ['C0', 'T']

    traits_view = View(
        Group(
            # Group(
            #     Item("TotalCounts", style='readonly', width=100, height=100, show_label=False, springy=True, full_size=True),
            # ),
            HGroup(
                Item('TracePlot', editor=ComponentEditor(), show_label=False),
                Group(
                    Item('c_enable0'), Item('c_enable1'), Item('c_enable2'),
                    Item('c_enable3'), Item('c_enable4'), Item('c_enable5'),
                    Item('c_enable6'), Item('sum_enable'),
                    label="Channels",
                    show_border=True,
                ),
            ),
            Group(
                Item('TraceLength'),
                Item('SecondsPerPoint'),
                Item('RefreshRate'),
                show_border=True,
            )
        ),
        title='Pi3diamond: Timetrace', width=600, height=500, x=10, y=30, buttons=[], resizable=True,
        handler=StartThreadHandler
    )


if __name__ == '__main__':
    import logging

    logging.getLogger().addHandler(logging.StreamHandler())
    logging.getLogger().setLevel(logging.DEBUG)
    logging.getLogger().info('Starting logger.')

    from tools.emod import JobManager

    JobManager().start()

    p = PhotonTimeTrace()
    p.edit_traits()
