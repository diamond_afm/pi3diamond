"""
This code is currently broken

small changes are required to adapt it to the new pulsed measurements (without hardware API).

See rabi.py as example.
"""

import numpy as np
from chaco.api import ArrayPlotData
from traits.api import Range, Float, Array
from traitsui.api import View, Item, Tabbed, HGroup, VGroup

from pulsed import Pulsed
from pulsed import PulsedTau
from rabi import Rabi
from tools.chaco_addons import SavePlot as Plot, SaveTool


class T1(PulsedTau):
    """Defines a T1 measurement."""

    laser = Float(default_value=3000., desc='laser [ns]', label='laser [ns]', mode='text',
                  auto_set=False,
                  enter_set=True)
    decay_init = Float(default_value=1000.,
                       desc='time to let the system decay after laser pulse [ns]',
                       label='decay init [ns]', mode='text', auto_set=False, enter_set=True)
    decay_read = Float(default_value=0.,
                       desc='time to let the system decay before laser pulse [ns]',
                       label='decay read [ns]', mode='text', auto_set=False, enter_set=True)

    aom_delay = Float(default_value=0.,
                      desc='If set to a value other than 0.0, the aom triggers are applied\nearlier by the specified value. Use with care!',
                      label='aom delay [ns]', mode='text', auto_set=False, enter_set=True)

    def generate_sequence(self):
        tau = self.tau
        laser = self.laser
        sequence = [(['aom'], laser)]
        for t in tau:
            sequence += [([], t), (['detect', 'aom'], laser)]
        sequence += [([], 1000)]
        sequence += [(['sequence'], 100)]
        return sequence

    get_set_items = PulsedTau.get_set_items + ['laser', 'decay_init', 'decay_read', 'aom_delay']

    traits_view = View(VGroup(HGroup(Item('submit_button', width=-60, show_label=False),
                                     Item('remove_button', width=-60, show_label=False),
                                     Item('resubmit_button', width=-60, show_label=False),
                                     Item('priority', width=-30),
                                     Item('state', style='readonly'),
                                     Item('run_time', style='readonly', format_str='%.f'),
                                     Item('stop_time', format_str='%.f'),
                                     Item('stop_counts'),
                                     ),
                              HGroup(Item('filename', springy=True),
                                     Item('save_button', show_label=False),
                                     Item('load_button', show_label=False)
                                     ),
                              Tabbed(VGroup(
                                  HGroup(Item('laser', width=-80, enabled_when='state != "run"'),
                                         Item('decay_init', width=-80,
                                              enabled_when='state != "run"'),
                                         Item('decay_read', width=-80,
                                              enabled_when='state != "run"'),
                                         Item('aom_delay', width=-80,
                                              enabled_when='state != "run"'),
                                         ),
                                  HGroup(
                                      Item('tau_begin', width=-80, enabled_when='state != "run"'),
                                      Item('tau_end', width=-80, enabled_when='state != "run"'),
                                      Item('tau_delta', width=-80, enabled_when='state != "run"'),
                                  ),
                                  label='stimulation'),
                                  VGroup(HGroup(Item('record_length', width=-80,
                                                     enabled_when='state != "run"'),
                                                Item('bin_width', width=-80,
                                                     enabled_when='state != "run"'),
                                                ),
                                         label='acquisition'),
                                  VGroup(HGroup(Item('integration_width'),
                                                Item('position_signal'),
                                                Item('position_normalize'),
                                                ),
                                         label='analysis'),
                              ),
                              ),
                       title='Pi3diamond: T1 Measurement',
                       )


class SingletDecay(Pulsed):
    """Singlet Decay."""

    laser = Range(low=1., high=100000., value=3000., desc='laser [ns]', label='laser [ns]',
                  mode='text', auto_set=False,
                  enter_set=True)

    tau_begin = Range(low=0., high=1e8, value=0., desc='tau begin [ns]', label='tau begin [ns]',
                      mode='text',
                      auto_set=False, enter_set=True)
    tau_end = Range(low=1., high=1e8, value=300., desc='tau end [ns]', label='tau end [ns]',
                    mode='text',
                    auto_set=False, enter_set=True)
    tau_delta = Range(low=1., high=1e6, value=3., desc='delta tau [ns]', label='delta tau [ns]',
                      mode='text',
                      auto_set=False, enter_set=True)

    tau = Array(value=np.array((0., 1.)))

    def apply_parameters(self):
        """Overwrites apply_parameters() from pulsed. Prior to generating sequence, etc., generate the tau mesh."""
        self.tau = np.arange(self.tau_begin, self.tau_end, self.tau_delta)
        Pulsed.apply_parameters(self)

    def start_up(self):
        self.pulse_generator.Night()

    def shut_down(self):
        self.pulse_generator.Light()

    def generate_sequence(self):
        tau = self.tau
        laser = self.laser

        sequence = [(['sequence'], 100)]
        for t in tau:
            sequence += [([], t), (['detect', 'aom'], laser)]
        return sequence

    get_set_items = Pulsed.get_set_items + ['tau_begin', 'tau_end', 'tau_delta', 'laser', 'tau']

    traits_view = View(VGroup(HGroup(Item('submit_button', show_label=False),
                                     Item('remove_button', show_label=False),
                                     Item('resubmit_button', show_label=False),
                                     Item('priority'),
                                     Item('state', style='readonly'),
                                     Item('run_time', style='readonly', format_str='%.f'),
                                     ),
                              Tabbed(VGroup(
                                  HGroup(Item('laser', width=-80, enabled_when='state != "run"'), ),
                                  HGroup(
                                      Item('tau_begin', width=-80, enabled_when='state != "run"'),
                                      Item('tau_end', width=-80, enabled_when='state != "run"'),
                                      Item('tau_delta', width=-80, enabled_when='state != "run"'),
                                      Item('stop_time'), ),
                                  label='manipulation'),
                                  VGroup(HGroup(Item('record_length', width=-80,
                                                     enabled_when='state != "run"'),
                                                Item('bin_width', width=-80,
                                                     enabled_when='state != "run"'), ),
                                         label='detection'
                                         ),
                              ),
                              ),
                       title='Singlet decay',
                       )


class T1pi(Rabi):
    """Defines a T1 measurement with pi pulse."""

    double_sequence = True

    def generate_sequence(self):
        """
        Generate T1 Measurement sequence with and without applied pi pulse.
        
        Here we generate a logarithmic tau array, so we do not have to measure as many points at
        longer times between prepare and readout pulses, since this takes a lot of time!
        """
        self.tau = np.logspace(np.log10(self.tau_begin), np.log10(self.tau_end + self.tau_delta),
                               self.tau_delta)
        tau = self.tau
        laser = self.laser

        decay_init = self.decay_init
        mw_x_t_pi = self.mw_x_t_pi

        sequence = []
        for t in tau:
            sequence.append(([], t))
            sequence.append((['detect', 'aom'], laser))
            sequence.append(([], decay_init))
        for t in tau:
            sequence.append((['microwave'], mw_x_t_pi))
            sequence.append(([], t))
            sequence.append((['detect', 'aom'], laser))
            sequence.append(([], decay_init))
        sequence.append((['sequence'], 100))
        return sequence

    def _create_line_plot(self):
        line_data = ArrayPlotData(index=np.array((0, 1)),
                                  first=np.array((0, 0)),
                                  second=np.array((0, 0)),
                                  )
        plot = Plot(line_data, padding=8, padding_left=64, padding_bottom=36)
        plot.plot(('index', 'first'), color='blue', name='first', index_scale='log')
        plot.plot(('index', 'second'), color='green', name='second', index_scale='log')
        plot.index_axis.title = 'time [micro s]'
        plot.value_axis.title = 'spin state'
        plot.tools.append(SaveTool(plot))
        self.line_data = line_data
        self.line_plot = plot

    traits_view = View(
        VGroup(
            HGroup(Item('submit_button', show_label=False),
                                     Item('remove_button', show_label=False),
                                     Item('resubmit_button', show_label=False),
                                     Item('get_global_vars_button', show_label=False),
                                     Item('priority'),
                                     Item('state', style='readonly'),
                                     Item('run_time', style='readonly', format_str='%.f',
                                          width=-60),
                                     Item('stop_time', width=-60),
                                     Item('stop_counts', width=-60),
                                     ),
                              HGroup(Item('filename', springy=True),
                                     Item('save_button', show_label=False),
                                     Item('load_button', show_label=False),
                                     Item('periodic_save'),
                                     Item('save_interval', enabled_when='not periodic_save'),
                                     ),
                              Tabbed(VGroup(HGroup(
                                  Item('mw_frequency', width=-120, enabled_when='state != "run"'),
                                  Item('mw_power', width=-60, enabled_when='state != "run"'),
                                  Item('decay_init', width=-80, enabled_when='state != "run"'),
                                  Item('mw_x_t_pi', width=-80, enabled_when='state != "run"'), ),
                                  HGroup(Item('tau_begin', width=-80,
                                              enabled_when='state != "run"'),
                                         Item('tau_end', width=-80,
                                              enabled_when='state != "run"'),
                                         Item('tau_delta', width=-80,
                                              enabled_when='state != "run"'),
                                         ),
                                  label='acquisition'),
                                  VGroup(HGroup(Item('integration_width'),
                                                Item('position_signal'),
                                                Item('position_normalize'),
                                                ),
                                         label='analysis'),
                              ),
                              Tabbed(Item('line_plot', show_label=False, width=500, height=-300,
                                          resizable=True),
                                     Item('matrix_plot', show_label=False, width=500, height=-300,
                                          resizable=True),
                                     Item('pulse_plot', show_label=False, width=500, height=-300,
                                          resizable=True),
                                     ),
                              ),
        title='Pi3diamond: T1pi',
        resizable=True
    )

    get_set_items = Rabi.get_set_items + ['mw_x_t_pi']


if __name__ == '__main__':
    # from hardware.dummy import PulseGenerator, TimeTagger, Microwave
    # t1 = T1pi(analog_pulser, time_tagger, microwave,
    #           frequency=rabi.frequency,
    #           mw_power=rabi.mw_power,
    #           mw_x_t_pi=80
    #           )
    # t1.edit_traits()
    pass
