from pulse_generator import PulseGenerator


class CustomPulseGenerator(PulseGenerator):

    def Light(self):
        self.setContinuous(['aom'])

    def Open(self):
        self.setContinuous(['aom', 'mw'])

    def Night(self):
        self.setContinuous([])

    Sequence = PulseGenerator.setSequence
