/*
    backend for TimeTagger, an OpalKelly based single photon counting library
    Copyright (C) 2011  Markus Wick <wickmarkus@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef WRAPPER_SWIG_I
#define WRAPPER_SWIG_I
%module TimeTagger
// first section
%{
#include "TimeTagger.h"
#include "Iterator.cpp"
#include "Histogram.cpp"
#include "Pulsed.cpp"
#include "PulsedAFM.cpp"
#include "PulsedExtTrig.cpp"
#include "CountBetweenMarkers.cpp"
#include "Counter.cpp"

//#include "okFrontPanelDLL.cpp"

#include "numpy/arrayobject.h"
%}
// second section

%include "numpy.i"
%include "std_string.i"
%init %{
	import_array();
%}

%include "TimeTagger.h"
%include "Iterator.cpp"
%include "Histogram.cpp"
%include "Counter.cpp"
%include "Pulsed.cpp"
%include "PulsedAFM.cpp"
%include "PulsedExtTrig.cpp"
%include "CountBetweenMarkers.cpp"

//%include "okFrontPanelDLL.cpp"

#endif
